
#############################
  Translate the User Manual
#############################

Help bring Blender to the world! 🌍 Join the translation effort to make the Blender Manual accessible in more
languages. Whether you're fluent in another language or just starting, every contribution makes a difference. Let's
build a global community!

.. toctree::
   :maxdepth: 2

   contribute.rst
   style_guide.rst


Maintenance
===========

.. toctree::
   :hidden:

   add_language.rst

- :doc:`add_language`

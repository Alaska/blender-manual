
###################
  Getting Started
###################

The following guides lead you through the process.

.. toctree::
   :maxdepth: 1

   install/index.rst
   build.rst
   editing.rst
   pull_requests.rst

.. _bpy.types.TextureNodeTexture:

************
Texture Node
************

.. figure:: /images/node-types_TextureNodeTexture.webp
   :align: right
   :alt: Texture node.

The Texture node loads another node-based or non-node-based texture.


Inputs
======

These two colors can be used to remap a grayscale texture.

Color 1
   White Level.
Color 2
   Black Level.


Properties
==========

Texture
   The texture to load -- either from the current blend-file, or from a linked one.


Outputs
=======

Color
   Standard color output.

.. index:: Geometry Nodes; Object
.. _bpy.types.GeometryNodeInputObject:

***********
Object Node
***********

.. figure:: /images/node-types_GeometryNodeInputObject.png
   :align: right
   :width: 300px
   :alt: Object Input Node.

The *Object* input node outputs a single object. It can be connected to other object sockets
to make using the same object in multiple places more convenient.


Inputs
======

This node has no inputs.


Properties
==========

- Object


Output
======

Object
   A reference to the selected object.

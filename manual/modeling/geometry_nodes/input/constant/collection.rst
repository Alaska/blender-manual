.. index:: Geometry Nodes; Collection
.. _bpy.types.GeometryNodeInputCollection:

***************
Collection Node
***************

.. figure:: /images/node-types_GeometryNodeInputCollection.png
   :align: right
   :width: 300px
   :alt: Collection Input Node.

The *Collection* input node outputs a single collection. It can be connected to other collection sockets
to make using the same collection in multiple places more convenient.


Inputs
======

This node has no inputs.


Properties
==========

- Collection


Output
======

Collection
   A reference to the selected Collection.

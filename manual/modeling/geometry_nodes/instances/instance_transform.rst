.. index:: Geometry Nodes; Instance Transform
.. _bpy.types.GeometryNodeInstanceTransform:

***********************
Instance Transform Node
***********************

.. figure:: /images/node-types_GeometryNodeInstanceTransform.webp
   :align: right
   :alt: Instance Transform node.

The *Instance Transform* outputs the :term:`Transformation Matrix` of each top-level instance in the
local space of the modifier object.

The :doc:`/modeling/geometry_nodes/instances` page contains more information about geometry instances.


Inputs
======

This node has no inputs.


Properties
==========

This node has no properties.


Outputs
=======

Transformation
   Matrix that indicates the :term:`Transformation` of each top-level instance.
